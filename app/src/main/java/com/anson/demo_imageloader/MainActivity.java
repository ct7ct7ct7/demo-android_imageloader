package com.anson.demo_imageloader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class MainActivity extends AppCompatActivity {

    private static final String IMAGE_URL = "http://i.imgur.com/DvpvklR.png";

    private ImageView mImageView00;
    private ImageView mImageView01;
    private ImageView mImageView02;
    private ImageView mImageView03;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mImageView00 = (ImageView)findViewById(R.id.imageView00);
        mImageView01 = (ImageView)findViewById(R.id.imageView01);
        mImageView02 = (ImageView)findViewById(R.id.imageView02);
        mImageView03 = (ImageView)findViewById(R.id.imageView03);


        Glide.with(this)
                .load(IMAGE_URL)
                .error(android.R.drawable.stat_notify_error)
                .placeholder(android.R.drawable.ic_menu_gallery)
                .into(mImageView00);

        Picasso.with(this)
                .load(IMAGE_URL)
                .error(android.R.drawable.stat_notify_error)
                .placeholder(android.R.drawable.ic_menu_gallery)
                .into(mImageView01);

        new AQuery(this).id(mImageView02)
                .image(IMAGE_URL, true, true, 0, android.R.drawable.stat_notify_error);



        new AsyncTask<Void,Void,Bitmap>(){
            @Override
            protected Bitmap doInBackground(Void... params) {
                File file = new File(MainActivity.this.getCacheDir(),"test.jpg");
                Bitmap bitmap = loadBitmapFromUrl(IMAGE_URL, file);
                return bitmap;
            }
            @Override
            protected void onPostExecute(Bitmap bitmap) {
                mImageView03.setImageBitmap(bitmap);
                super.onPostExecute(bitmap);
            }
        }.execute();
    }


    /**下載URL上的檔案並轉成Bitmap*/
    public static Bitmap loadBitmapFromUrl(String url, File file) {
        Bitmap bitmap = null;
        try {
            URL downloadUrl = new URL(url);
            /* Open a connection to that URL. */
            URLConnection uCon = downloadUrl.openConnection();
            /*
             * Define InputStreams to read from the URLConnection.
             */
            InputStream is = uCon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            /*
             * Read bytes to the Buffer until there is nothing more to read(-1).
             */
            ByteArrayBuffer baf = new ByteArrayBuffer(5000);
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }
            /* Convert the Bytes read to a String. */
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(baf.toByteArray());
            fos.flush();
            fos.close();
            /* Create Bitmap. */
            bitmap = BitmapFactory.decodeFile(file.getPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
